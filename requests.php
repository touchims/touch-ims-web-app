<?php
session_start();
require_once('queries/dbconnect.php');
require_once('sync.php');
include('views/header_ims.php');
$db = new DBconnect();
$connection = $db->connect();
$requests = new Requests($connection);
if(!empty($_SESSION['username'])){
	echo $requests->createTable();
	//echo $requests->createApprovedTable();
}

if(!empty($_POST['approve'])){
	//echo "approve";
	$requests->updateRequest('approved',$_POST['approve']);

}

if(!empty($_POST['decline'])){
	//echo "decline";
	$requests->updateRequest('declined',$_POST['decline']);
}

class Requests{
	private $connection;
	function __construct($connection){
		$this->connection = $connection;
	}

	function updateRequest($approval,$request_id){
		$query = "
		update touch_room_transfer_requests set approval='$approval'
		where request_id = '$request_id'
		";
		$result = mysqli_query($this->connection, $query);

		if($result){
			echo "<script>alert('Request Approved')</script>";
			$sync = new Sync($this->connection);
			$sync->insertSync('Request Approved',$_SESSION['username']);
		}
		else{
			echo "<script>alert('Request Declined')</script>";
			$sync = new Sync($this->connection);
			$sync->insertSync('Request Declined',$_SESSION['username']);
		}

		header('location:requests.php');

		
	}

	function getApprovedRequests(){
		//$username = $_SESSION['username'];
		
		//echo $username;
		$query = "
		select request_id, request_date, offer_code, room_no, reason, duration, date_effective from touch_room_transfer_requests
		where approval = 'approved'
		";
		//echo $query;
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;

		}
		return $results;
	}


	function getRequests(){
		//$username = $_SESSION['username'];
		
		//echo $username;
		$query = "
		select request_id, request_date, offer_code, room_no, reason, duration, approval, date_effective from touch_room_transfer_requests
		";
		//echo $query;
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;

		}
		// echo "<pre>";
		// print_r($results);
		// echo "</pre>";
		return $results;
	}

	function createTable(){
		$requests = $this->getRequests();
		//$room_target = $this->getRooms();
		$string = "";

		$string = "<form method=post> 
		<table align=center>
		<tr> <th colspan=9> Pending Requests</th> <tr>
		<tr>
		<th>Request#</th>
		<th>Request Date </th>
		<th>Date Effective </th>
		<th>Offer Code</th>
		<th>Room No</th>
		<th>Reason</th>
		<th>Duration</th>
		<th colspan=2>Approval</th>
		
	
		</tr>
		";

		foreach($requests as $row){
			$request_id = $row['request_id'];
			$request_date = $row['request_date'];
			$date_effective = $row['date_effective'];
			$offer_code = $row['offer_code'];
			$room_no = $row ['room_no'];
			$reason = $row ['reason'];
			$duration = $row ['duration'];
			$approval = $row['approval'];
			$string.= 
			"<tr>
			<td>$request_id</td>
			<td>$request_date</td>
			<td>$date_effective</td>
			<td>$offer_code</td>
			<td>$room_no</td>
			<td>$reason</td>
			<td>$duration</td>";

			if($approval==null){
				$string.="<td> <input type='image' src='images/b_edit.png' border='0' alt='Submit' name='approve' value='$request_id' /> </td>
				<td> <input type='image' src='images/b_drop.png' border='0' alt='Submit' name='decline' value='$request_id'/> </td>";
			}
			else{
				$string.="<td colspan=2>$approval</td>";
			}

			$string.="</tr>";
		}
		$string.= "</table>
		</form>";
		return $string;
	}
	function createApprovedTable(){
		$requests = $this->getApprovedRequests();
		//$room_target = $this->getRooms();
		$string = "";

		$string = "<form method=post> 
		<table align=center>
		<tr> <th colspan=8> Approved Requests</th> <tr>
		<tr>
		<th>Request#</th>
		<th>Request Date </th>
		<th>Offer Code</th>
		<th>Room No</th>
		<th>Reason</th>
		<th>Duration</th>
		<th> </th>
		<th> </th>
	
		</tr>
		";

		foreach($requests as $row){
			$request_id = $row['request_id'];
			$request_date = $row['request_date'];
			$offer_code = $row['offer_code'];
			$room_no = $row ['room_no'];
			$reason = $row ['reason'];
			$duration = $row ['duration'];
			$string.= 
			"<tr>
			<td>$request_id</td>
			<td>$request_date</td>
			<td>$offer_code</td>
			<td>$room_no</td>
			<td>$reason</td>
			<td>$duration</td>

			<td> x </td>
			<td> x </td>
	



			</tr>";
		}
		$string.= "</table>
		</form>";
		return $string;
	}


}


?>

<style>
td,th{
	text-align: center;
}
</style>