

<?php
session_start();
require_once('queries/dbconnect.php');
include('views/header_faculty.php');
$db = new DBconnect();
$connection = $db->connect();
$roomtransfer = new RoomTransfer($connection);

if(!empty($_SESSION['username'])){
	echo "<div> </div>";
	echo "<div id=sched >".$roomtransfer->createLoadsTable()."</div>";
	echo "<div id=pending>".$roomtransfer->createRequestsTable('pending')."</div>";
	echo "<div id=approved>".$roomtransfer->createRequestsTable('approved')."</div>";
	echo "<div id=declined>".$roomtransfer->createRequestsTable('declined')."</div>";
	
}
else{
	header('location: index.php');
}


if(!empty($_SESSION['username']) && !empty($_POST['request_id'])){
	$roomtransfer->deleteRequest($_POST['request_id']);

}





class RoomTransfer{
	private $connection;
	function __construct($connection){
		$this->connection = $connection;
	}

	function deleteRequest($request_id){
		$query= ("delete from touch_room_transfer_requests where request_id='$request_id'");
		echo $query;
		mysqli_query($this->connection,$query);
	}

	function createRequestsTable($approval){

		
		if($approval=='pending'){
			$where_approval='is null';
			$table_title = "Pending Requests";
			
		}
		elseif($approval=='approved'){
			$where_approval="='approved'";
			$table_title = "Approved Requests";
		}
		elseif($approval=='declined'){
			$where_approval="='declined'";
			$table_title = "Declined Requests";
		}

		$requests = $this->getRequests($where_approval);
		
		$string = "";

		$string = "<table align=center>
		<tr> <th colspan=7> $table_title </th> </tr>
		<tr>
		<th>Request#</th>
		<th>Request Date </th>
		<th>Effective From </th>
		<th>Effective To </th>
		<th>Offer Code</th>
		<th>Room No</th>
		<th>Reason</th>
	
	
		</tr>
		";

		foreach($requests as $row){
			$onclick=null;
			$request_id = $row['request_id'];
			if($approval=="pending")
				$onclick= "cancelRequest($request_id)";
			$request_date = $row['request_date'];
			$date_effective = $row['date_effective'];
			$date_effective2 = $row['date_effective_until'];
			$offer_code = $row['offer_code'];
			$room_no = $row ['room_no'];
			$reason = $row ['reason'];
			//$duration = $row ['duration'];
			$string.= 
			"<tr onclick=$onclick id='values'>
			<td>$request_id</td>
			<td>$request_date</td>
			<td>$date_effective</td>
			<td>$date_effective2</td>

			<td>$offer_code</td>
			<td>$room_no</td>
			<td>$reason</td>
			
			</tr>";
		}
		$string.= "</table>";
		return $string;
	}

	function getRequests($approval){
		//$username = $_SESSION['username'];
		
		//echo $username;
		$query = "
		select request_id, request_date, offer_code, room_no, reason,date_effective, date_effective_until from touch_room_transfer_requests
		where approval $approval
		";
		//echo $query;
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;

		}
		// echo "<pre>";
		// print_r($results);
		// echo "</pre>";
		return $results;
	}

	function getLoads(){
		
		$username = $_SESSION['username'];
		
		//echo $username;
		$query = "select p.last_name, p.first_name,p.person_id, t.teacher_id, 
		ss.offer_code , ss.day, ss.time, ss.room_no, s.subj_name
		from person p, teacher t, subj_schedule ss, subject s
		where p.person_id = t.person_id and t.teacher_id = ss.teacher_id and ss.subj_id=s.subj_id
		and p.person_id = (select person_id from touch_ims_accounts where username='$username')
		";
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;

		}
		return $results;
	}

	function createLoadsTable(){
		$loads = $this->getLoads();
		//$room_target = $this->getRooms();
		$string = "";

		$string = "<table class='table table-hover' align=center>
		<tr><th colspan=6> Teacher's Load </th></tr>
		<tr>
		<th>Offer Code</th>
		<th>Course No</th>
		<th>Time </th>
		<th>Day </th>
		<th>Room </th>
		<th> </th>
	
		</tr>
		";

		foreach($loads as $row){
			$offercode = $row['offer_code'];
			$courseno = $row['subj_name'];
			$time = $row['time'];
			$day = $row['day'];
			$room = $row ['room_no'];
			$string.= 
			"<tr id=values >
			<td>$offercode</td>
			<td>$courseno</td>
			<td>$time</td>
			<td>$day</td>
			<td>$room</td>
			<td> <a href='room_transfer_form.php?offercode=$offercode'>x</a> </td>
			</tr>";
		}
		$string.= "</table>";
		return $string;
	}

}
?>

<script>
function cancelRequest(request_id) {
    var x;
    if (confirm("Delete request_id "+request_id+"?") == true) {
    	ajaxRequest(request_id,"delete");
        //x = "You pressed OK!";
    } else {
    	ajaxRequest(request_id);
        //x = "You pressed Cancel!";
    }
    //document.getElementById("demo").innerHTML = x;
    window.location.reload();
}

function ajaxRequest(request_id,response){
	//alert(request_id);
	var xmlhttp;
	xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function()
	{


		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			//document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
			//alert(responseText);
		}

	}

	xmlhttp.open("POST","room_transfer.php",true);
	//xmlhttp.open("GET","queries/reportsync.php",false);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('action='+response+'&request_id='+request_id);	
}

</script>

<style>
#container {outline:1px solid red;}
th, td { padding: 2px; }
td { text-align: center;}

#buttons {width:120px;outline: 1px solid blue;margin:0 auto;}
table{width:680px;outline:1px solid green;}
#values:hover{
	background-color: green;
}

</style>










