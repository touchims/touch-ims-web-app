<?php
session_start();
require_once('queries/dbconnect.php');
require_once('views/header_faculty.php');

$db = new DBconnect();
$connection = $db->connect();
$faculty = new FacultyReports($connection);

$faculty->createTable();


class FacultyReports{
	private $connection;



	public function __construct($connection){
		$this->connection = $connection;
	}

	function query(){
		$username = $_SESSION['username'];
		
		$query = "
		select reports.offer_code , p.person_id , reports.date, reports.first_checking, reports.report_id,
		reports.faculty_feedback, reports.checker1, reports.checker2,
		time(reports.checker1_time)as time1, reports.last_checking,
		time(reports.checker2_time)as time2 
		from touch_faculty_report reports 
		inner join subj_schedule ss on reports.offer_code = ss.offer_code
		inner join teacher t on t.teacher_id = ss.teacher_id
		inner join person p on p.person_id = t.person_id
		inner join touch_ims_accounts ti on ti.person_id = p.person_id
		where ti.username = '$username' and (first_checking in ('late','absent') 
		or last_checking in ('absent','early dismissal')) and reports.checking_status!='0/2'
		
		";
		//echo $query;
		$result = mysqli_query($this->connection,$query);
		$rows=mysqli_num_rows($result);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}

		// echo "<pre>";
		// print_r($results);
		// echo "</pre>";
		return $results;

	}
	function createTable(){
		$results = $this->query();
		
		if(sizeof($results)>0){
			echo "<table border>";
			echo
			"<tr>
			<th> offer code</th> 
			<th> date </th> 
			<th>Class Start</th>  
			<th>Time In</th>
			<th>Dismissal</th>
			<th>Time Out</th> 
			<th>feedback</th>  
			</tr>";
			foreach($results as $row){
				$offercode = $row['offer_code'];
				$date = $row['date'];
				$mark1 = $row['first_checking'];
				$time1 = $row['time1'];
				$mark2 = $row['last_checking'];
				$time2 = $row['time2'];
				$report_id = $row['report_id'];
				$ff = $row['faculty_feedback'];
				
				echo "
				<tr>
				<td>$offercode</td>
				<td>$date</td>
				<td>$mark1</td>
				<td>$time1</td>
				<td>$mark2</td>
				<td>$time2</td>";

				if(empty($ff)){
					 echo "<td><a href='feedback.php?report_id=$report_id'>send feedback</a></td> </tr>";
				}
				else
					 echo "<td>$ff</td> </tr>";

				
				//echo $html;
			}
			echo "</table>";
		}
	}
}
?>

<style>
table{
	
	width:800px;
	margin:auto;
}
</style>