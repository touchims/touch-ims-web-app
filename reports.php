<?php
include('views/header_ims.php');
require_once('queries/dbconnect.php');
session_start();
$db = new DBConnect();
$connection = $db->connect();
$reports = new Reports($connection);
class Reports{
	private $connection;

	public function __construct($connection){
		$this->connection = $connection;
	}
	function query($query){
		
		$result = mysqli_query($this->connection,$query);
		$rows=mysqli_num_rows($result);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}

		return $results;
	}
	function queryAll(){
		return "SELECT date,first_checking,last_checking,checker1,checker2,last_name,first_name,faculty_feedback,
		time(checker2_time)as time2,time(checker1_time)as time1,
		substr(time,-13,5) as time_start, substr(time,-5) as time_end
		FROM touch_faculty_report tfr, person p, subj_schedule ss, teacher t where
		checking_status!='0/2' and
		tfr.offer_code=ss.offer_code and t.person_id = p.person_id and ss.teacher_id = t.teacher_id ";
	}
	function queryViolations(){

		return 'SELECT date,first_checking,last_checking,checker1,checker2,last_name,first_name,faculty_feedback,
		time(checker2_time)as time2,time(checker1_time)as time1,
		substr(time,-13,5) as time_start, substr(time,-5) as time_end
		FROM touch_faculty_report tfr, person p, subj_schedule ss, teacher t
		WHERE (first_checking!="PRESENT" or last_checking!="OK") and (checking_status!="0/2") and

		tfr.offer_code=ss.offer_code and t.person_id = p.person_id and ss.teacher_id = t.teacher_id';
	}
	function viewReports($quer,$title){
		$query = $this->query($quer);
		//print_r($quer) ;
		if($query){
			
			echo "<table  >
			<tr> <td colspan=10>$title</td></tr>
			<tr class=field>
			<td class='left'> Last Name </td>
			<td > First Name </td>
			<td > Start Class </td>
			<td > End Class </td>
			<td > Date </td>
			<td > Checker 1 </td>
			<td > Time IN </td>
			<td > Checker 2 </td>
			<td > Time Out </td>
			<td> Feedback </td>
			</tr>";
			foreach($query as $row){
			//$offercode = $row['offer_code'];
				$transaction1 = $row['first_checking'];
				$transaction2 = $row['last_checking'];
				$date= $row['date'];
				$checker1 = $row['checker1'];
				$checker2 = $row['checker2'];
				$lastname = $row['last_name'];
				$firstname = $row['first_name'];
				$timein = $row['time1'];
				$timeout = $row['time2'];
				$feedback = $row['faculty_feedback'];

				$time_start = $row['time_start'];
				$time_end = $row['time_end'];

				//echo $time_start;

				if($transaction1 == "LATE" or $transaction1 == "ABSENT"){
					$transaction1 = "<b style='color:blue'> $transaction1</b>";
				}
				if($transaction2 == "EARLY DISMISSAL" or $transaction2 == "ABSENT"){
					$transaction2 = "<b style='color:blue'> $transaction2</b>";
				}
				if($transaction1 == null){
	 				$current_date = date('Y-m-d');
	 				$report_date = date('Y-m-d',$date);


	 				if($current_date > $report_date){
	 					$transaction1 = "test";
	 				}
				}

				if($transaction2 == null){
	 				$current_date = date('Y-m-d');
	 				$report_date = date($date);
	 				$current_time = date('G:i:s');
	 				$report_time = date($time_end);
	 				if($current_date <= $report_date && $current_time <= $report_time){
	 					$transaction2 = "PENDING";
	 				}
	 				else
	 					$transaction2= "NOT CHECKED";
				}




				echo "
				<tr class=data>
				<td class='left'> $lastname </td>
				<td> $firstname </td>
				<td> $transaction1 </td>
				<td> $transaction2 </td>
				<td> $date </td>
				<td> $checker1 </td>
				<td> $timein </td>
				<td> $checker2 </td>
				<td> $timeout </td>
				<td> $feedback </td>
				</tr>";
			}
			echo "</table>";


		}
	}
}
?>
<meta charset="utf-8">
<title>jQuery UI Datepicker - Default functionality</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<div id='container1'>
	<form method=post style="1px solid red">
		<select name='reports' id='reports'>
			<option disabled selected>select report</option>
			<option value=1>All</option>
			<option value=2>With Violations</option>
		</select>
		From<input name='datepickerfrom' id='datepickerfrom' /> To <input name='datepickerto' id='datepickerto' />
		<input type=reset />
		<input type=submit name=submit/>
	</form>
	<?php
	$query = $reports->queryAll(); 
	$where = "";
	$title = "All Reports";



	if(!empty($_POST['reports'])) {
		if($_POST['reports']=="2"){
			$query = $reports->queryViolations(); 
			$title = "Reports with violations";
		}
		if($_POST['reports']=="1"){
			$query = $reports->queryAll(); 
			$title = "All Reports";
		}

		$_SESSION['report_type']=$_POST['reports'];

		if(!empty($_POST['datepickerfrom']) && empty($_POST['datepickerto'])){
			$from = $_POST['datepickerfrom'];
			$where = " and date >= '$from'";
			$title.=" from $from";

		}
		elseif(!empty($_POST['datepickerto']) && empty($_POST['datepickerfrom'])){
			$to = $_POST['datepickerto'];
			$where = " and date <= '$to'";
			$title.=" from $to";

		}
		elseif(!empty($from = $_POST['datepickerto']) && !empty($to = $_POST['datepickerfrom']) ){
			$where = " and date between '".$_POST['datepickerfrom']."' and '".$_POST['datepickerto']."'";
			$title.=" from $from to $to";
		}
		$reports->viewReports($query.$where,$title);
	}
	// else
	// 	echo "<script>alert('select report type')</script>";




	?>
</div>





<style>

#container1{
	width:100%;
	height:100%;
	margin-top:10px;
}
form{

	width:670px;
	margin:auto;
}
table{

	text-align: center;
	margin:0 auto;
	background-color: #827B85;
	
}
td{
	padding:5px 5px 5px 5px;

}

tr{

	outline:1px solid black;
}

.field{
	background-color: #454243;
	color:white;

}


</style>

<script>
$(function() {
	$( "#datepickerfrom,#datepickerto" ).datepicker({
		dateFormat: "yy-mm-dd"
	});

});
</script>