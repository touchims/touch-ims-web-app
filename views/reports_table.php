<?php

require_once('../queries/dbconnect.php');


$db = new DBConnect();
$connection = $db->connect();
$reports = new Reports($connection);



class Reports{

	private $connection;

	public function __construct($connection){
		$this->connection = $connection;
	}

	function query($query){
		
		$result = mysqli_query($this->connection,$query);
		$rows=mysqli_num_rows($result);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}

		return $results;

	}

	function viewReports(){
		$quer = 'SELECT date,first_checking,last_checking,checker1,checker2,last_name,first_name,faculty_feedback,
		time(checker2_time)as time2,time(checker1_time)as time1
			FROM touch_faculty_report tfr, person p, subj_schedule ss, teacher t
			WHERE (first_checking!="PRESENT" or last_checking!="OK") and (checking_status!="0/2") and

			 tfr.offer_code=ss.offer_code and t.person_id = p.person_id and ss.teacher_id = t.teacher_id order by date desc';
		$query = $this->query($quer);
		//print_r($quer) ;
		if($query){
			
			echo "<table  >
			<tr class=field>
			<td class='left'> Last Name </td>
			<td > First Name </td>
			<td > Start Class </td>
			<td > End Class </td>
			<td > Date </td>
			<td > Checker 1 </td>
			<td > Time IN </td>
			<td > Checker 2 </td>
			<td > Time Out </td>
			<td> Feedback </td>
			</tr>";
			foreach($query as $row){
			//$offercode = $row['offer_code'];
				$transaction1 = $row['first_checking'];
				$transaction2 = $row['last_checking'];
				$date= $row['date'];
				$checker1 = $row['checker1'];
				$checker2 = $row['checker2'];
				$lastname = $row['last_name'];
				$firstname = $row['first_name'];
				$timein = $row['time1'];
				$timeout = $row['time2'];
				$feedback = $row['faculty_feedback'];

				echo "
				<tr class=data>
				<td class='left'> $lastname </td>
				<td> $firstname </td>
				<td> $transaction1 </td>
				<td> $transaction2 </td>
				<td> $date </td>
				<td> $checker1 </td>
				<td> $timein </td>
				<td> $checker2 </td>
				<td> $timeout </td>
				<td> $feedback </td>
				</tr>";
			}
			echo "</table>";
		}
	}

}






?>


<div id='container1'>
<?php 
	$reports->viewReports(); 
?>
</div>





<style>

#container1{
	width:100%;
	height:100%;
	margin-top:100px;
}
table{

	text-align: center;
	margin:0 auto;
		background-color: #827B85;
	
}
td{
	padding:5px 5px 5px 5px;

}

tr{

	outline:1px solid black;
}

.field{
	background-color: #454243;
	color:white;

}


</style>