<?php
require_once('queries/dbconnect.php');
include('views/header_ims.php');

if(isset($_POST['datepicker'])){
  echo $datepicker = $_POST['datepicker'];
}

$db = new DBconnect();
$connection = $db->connect();
$review = new Review($connection);


class Review{
  private $connection;

  function __construct($connection){
    $this->connection = $connection;

  }

  function queryReports(){
    $query = "select date, offer_code, first_checking, last_checking, checking_status, checker1, checker2
    from touch_faculty_report  where checking_status='2/2' order by date";
    $result = mysqli_query($this->connection,$query);
    $results = array();

    //echo $query;

    while($line = mysqli_fetch_assoc($result)){
      $results[] = $line;

    }
    return $results;
  }

  function createTable(){
    $results = $this->queryReports();
    //print_r($results);
    echo "<table>";
    echo 
    "<tr> 
    <td> Date</td>
    <td> Offer Code</td>
    <td> Transaction 1</td>
    <td> Transaction 2</td>
    <td> Checker 1</td>
    <td> Checker 2</td>
    </tr>";

    foreach($results as $row){
      $date = $row['date'];
      $offercode = $row['offer_code'];
      $firstchecking = $row['first_checking'];
      $lastchecking = $row['last_checking'];
      $checker1 = $row['checker1'];
      $checker2 = $row['checker2'];

      echo 
      "<tr> 
      <td> $date</td>
      <td> $offercode</td>
      <td> $firstchecking</td>
      <td> $lastchecking</td>
      <td> $checker1</td>
      <td> $checker2</td>
      </tr>";


    }

    echo "</table>";


  }


}

?>


<meta charset="utf-8">
<title>jQuery UI Datepicker - Default functionality</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(function() {
  $( "#datepicker" ).datepicker({
    dateFormat: "yy-mm-dd"
  });

});
</script>





<form method=post />
Date: <input type="text" id="datepicker" name='datepicker' /s> <input type=submit value="go" />
</form>

<?php
$review->createTable();
?>