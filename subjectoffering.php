<?php
include('views/header_ims.php');
require_once('queries/dbconnect.php');
$db = new DBconnect();
$connection = $db->connect();
$subjectoffering = new SubjectOffering($connection);
$conflict_count = 0;




if( !empty($_POST['offercode']) 
	&& !empty($_POST['courseno']) 
	&& !empty($_POST['day']) 
	&& !empty($_POST['timestart']) 
	&& !empty($_POST['timeend'])
	&& !empty($_POST['room']) 
	&& !empty($_POST['faculty'])){

	$conflict_offer = $subjectoffering->checkOfferCode($_POST['offercode']);
	if($conflict_offer['count'] > 0){
		header('location: subjectoffering.php?error=1');
		//echo "offercode conflict";
	}
	else{

		$days = $_POST['day'];
		$day_concat = "";
		$where="(";
		foreach($days as $row){
			$day_concat.=$row;
			$percent = '%';
			if($row=='T'){
				$row="T%' and day not like 'Th%' and day not like 'MTh%' and day not like 'MWTh%' and day not like 'WTh%";
				$percent = '';
			}
			$where.="day like '%$row".$percent."' or ";
		}
		$day=substr($where, 0,-3).")";
		$room = $_POST['room'];
		$timestart = $_POST['timestart'];
		$timeend = $_POST['timeend'];
		$teacher_id = $_POST['faculty'];
		$conflict = $subjectoffering->checkNotAvailableRoom($day,$room,$timestart,$timeend); //offer nga ni-conflict part1
		$conflicts = $subjectoffering->getConflicts($conflict,$room,$teacher_id); //offer nga ni-conflict part2
		//conflict sa room
		if(sizeof($conflicts)> 0){
				$subjectoffering->createTable($conflicts);
		} 
		else{
			$offer_details = array(
					'offercode' => $_POST['offercode'],
					'courseno' => $_POST['courseno'],
					'day' => $day_concat,
					'timestart' => $_POST['timestart'],
					'timeend' => $_POST['timeend'],
					'room' => $_POST['room'],
					'teacher_id' => $_POST['faculty']
					);	
			$conflicts = $subjectoffering->conflictFaculty($offer_details,$day); //faculty
			//conflict sa faculty
			if(sizeof($conflicts)>0){
				$subjectoffering->createTable($conflicts);

			}
			else{
				$subjectoffering->insertOffer($offer_details);
			}
		}


	}
}



// else{
// 	echo "<script> alert('incomplete fields');</script>";
// }



class SubjectOffering{
	private $connection;
	function __construct($connection){
		$this->connection = $connection;
	}

	function createTable($results){

		echo sizeof($results);
		?>
		<table class="table table-hover">
			<tr> <th colspan=8>Schedule Conflict</th> </tr>
			<tr> 
				<th>Offer Code</th> 
				<th>CourseNo </th>
				<th>Description </th>
				<th>Day </th>
				<th>Class Start</th>
				<th>Class End</th>
				<th>Room No </th>
				<th>Faculty </th>
			</tr>
			<?php

			foreach($results as $row){
				$offercode = $row['offer_code'];
				$courseno = $row['subj_name'];
				$description = $row['subj_desc'];
				$day = $row['day'];
				$time1 = $row['time_start'];
				$time2 = $row['time_end'];
				$room = $row['room_no'];
				$fullname = $row['last_name'].", ".$row['first_name'];
				echo "
				<tr>
				<td> $offercode </td>
				<td> $courseno </td>
				<td> $description </td>
				<td> $day </td>
				<td> $time1 </td>
				<td> $time2 </td>
				<td> $room </td>
				<td> $fullname </td>
				</tr>
				";
			}
			echo "</table>";
	}

	function conflictFaculty($offer_details,$day){
		//$like = substr($where, 0,-3).")";
		$teacher_id = $offer_details['teacher_id'];
		$timestart = $offer_details['timestart'];
		$timeend = $offer_details['timeend'];
		$query = "select offer_code, day, teacher_id, time from subj_schedule
			where $day
			and teacher_id='$teacher_id' and 
			(
			(('$timestart'>=substr(time,-13,5) and '$timestart' < substr(time,-5))
			or 
			('$timeend'>substr(time,-13,5) and '$timeend' <= substr(time,-5)))
			or
			((substr(time,-13,5)>='$timestart' and substr(time,-13,5)<'$timeend')
			or
			(substr(time,-5)<'$timestart' and substr(time,-5)<'$timeend')))";
		//echo $query;
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}
		$concat="";
		foreach($results as $row){
			$concat.=$row['offer_code'].",";
		}
		$concat.=substr($concat, 0,-1);
		$query = "select ss.offer_code, s.subj_name, s.subj_desc, ss.day, 
		substr(ss.time,-13,5) as time_start, substr(ss.time,-5) as time_end,
		ss.room_no , 
		p.last_name,p.first_name
		from subj_schedule ss
		inner join subject s on ss.subj_id = s.subj_id
		inner join teacher t on t.teacher_id = ss.teacher_id
		inner join person p on p.person_id = t.person_id
		where ss.offer_code in ($concat)";
		//echo $query;
		if(!empty($concat)) {
			$result = mysqli_query($this->connection,$query);
			$results = array();
			while($line = mysqli_fetch_assoc($result)){
				$results[] = $line;
			}
			return $results;
		}	
	}


	function insertOffer($offer){
		$offercode = $offer['offercode'];
		$courseno = $offer['courseno'];
		$day = $offer['day'];
		$time = $offer['timestart'].' - '.$offer['timeend'];
		$room = $offer['room'];
		$teacher_id= $offer['teacher_id'];

		$query = "INSERT INTO subj_schedule
		(offer_code,school_year,semester,day,time,room_no,teacher_id,subj_id)values
		('$offercode','2014-2015','2','$day','$time','$room','$teacher_id','$courseno')";
		$result = mysqli_query($this->connection,$query);
		if($result){
			echo "<script> alert('Success')</script>";
		}
		else
			echo "<script> alert('Failed')</script>";
		
	}
	function checkOfferCode($offer_code){
		$query = "select count(*) as count from subj_schedule where offer_code=$offer_code";

			//echo $select;
		$result = mysqli_query($this->connection, $query);
		$count = mysqli_fetch_assoc($result);
			//echo ($count['count']);
		return $count;
	}
	function viewTeachers(){


		$html="<select name='faculty' >";
		foreach($this->getTeachers() as $row){

			$lastname = $row['last_name'];
				//echo $lastname."<br>";
			$firstname = $row['first_name'];
			$teacher_id = $row['teacher_id'];
				//echo $teacher_id."<br>";
			$html.="<option value=$teacher_id >$lastname, $firstname</option>";
		} $html."</select>";
		return $html;
	}
	function getTeachers(){

		$query = "select p.last_name, p.first_name,t.teacher_id from person p
		inner join teacher t on t.person_id = p.person_id";
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}
		return $results;
	}
	function viewCourseNo(){


		$html="<select name='courseno' >";
		foreach($this->getCourseNo() as $row){
			$subj_id = $row['subj_id'];
			$subj_name = $row['subj_name'];
			$subj_name."<br>";
			$html.="<option value=$subj_id >$subj_name</option>";
		} 
		$html.="</select>";
		return $html;
	}
	function getCourseNo(){

		$query = "select subj_name, subj_id from subject";
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}
		return $results;
	}
	function getRooms(){

		return array('BCL1','BCL2','BCL3','BCL4','BCL5',
			'BCL6','BCL7','BCL8','BCL9');
	}
	function getFaculty(){
	}
	function getConflicts($conflict,$room,$teacher_id){ //subj_schedule gikan

		$conf_codes="";
		for($i=0;$i<sizeof($conflict);$i++){
			$conf_codes.=$conflict[$i]['offer_code'].",";
		}
		$conf_codes=substr($conf_codes,0, -1);


		$query = "select ss.offer_code, s.subj_name, s.subj_desc, ss.day, 
		substr(ss.time,-13,5) as time_start, substr(ss.time,-5) as time_end,
		ss.room_no , 
		p.last_name,p.first_name
		from subj_schedule ss
		inner join subject s on ss.subj_id = s.subj_id
		inner join teacher t on t.teacher_id = ss.teacher_id
		inner join person p on p.person_id = t.person_id
		where ss.offer_code in ($conf_codes) and ss.room_no='$room'";
		//echo $query;
		
		if(!empty($conf_codes)) {
			$result = mysqli_query($this->connection,$query);
			$results = array();
			while($line = mysqli_fetch_assoc($result)){
				$results[] = $line;
			}
			return $results;
		}	
	}
	function getSubjects(){
		$query = "select ss.offer_code, s.subj_name, s.subj_desc, ss.day, 
		substr(ss.time,-13,5) as time_start, substr(ss.time,-5) as time_end,

		ss.room_no , 
		p.last_name,p.first_name
		from subj_schedule ss
		inner join subject s on ss.subj_id = s.subj_id
		inner join teacher t on t.teacher_id = ss.teacher_id
		inner join person p on p.person_id = t.person_id";
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}
		return $results;
	}
	function checkNotAvailableRoom($day,$room,$timestart,$timeend){
		$query = "select offer_code, day, room_no, time from subj_schedule
		where $day
		and room_no='$room' and 
		(
		(('$timestart'>=substr(time,-13,5) and '$timestart' < substr(time,-5))
		or 
		('$timeend'>substr(time,-13,5) and '$timeend' <= substr(time,-5)))
		or
		((substr(time,-13,5)>='$timestart' and substr(time,-13,5)<'$timeend')
		or
		(substr(time,-5)<'$timestart' and substr(time,-5)<'$timeend')))
		";

		//echo $query;
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}

		return $results;
	}
}

?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" href="http://jdewit.github.io/bootstrap-timepicker/css/bootstrap.min.css" />
<link type="text/css" href="http://jdewit.github.io/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="http://jdewit.github.io/bootstrap-timepicker/js/bootstrap-2.2.2.min.js"></script>
<script type="text/javascript" src="http://jdewit.github.io/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>



<?php
if(isset($_POST['offercode']) && $conflict_count>0) { //naay conflict nga offer
	?>
	<div>
		<form method=post>
			<table class="table table-hover">
				<tr> <th colspan=8>Schedule Conflict</th> </tr>
				<tr> 
					<th>Offer Code</th> 
					<th>CourseNo </th>
					<th>Description </th>
					<th>Day </th>
					<th>Class Start</th>
					<th>Class End</th>
					<th>Room No </th>
					<th>Faculty </th>
				</tr>
				<?php
				foreach($getconflicts as $row){
					$offercode = $row['offer_code'];
					$courseno = $row['subj_name'];
					$description = $row['subj_desc'];
					$day = $row['day'];
					$time1 = $row['time_start'];
					$time2 = $row['time_end'];
					$room = $row['room_no'];
					$fullname = $row['last_name'].", ".$row['first_name'];
					echo "
					<tr>
					<td> $offercode </td>
					<td> $courseno </td>
					<td> $description </td>
					<td> $day </td>
					<td> $time1 </td>
					<td> $time2 </td>
					<td> $room </td>
					<td> $fullname </td>
					</tr>
					";
				}

				?>

			</table>
		</form>
	</div>



	<?php
}

?>


<?php
//all offered subjects
if(!$_GET){
	?>

	<div>
		<form method=post>
			<table class="table table-hover">
				<tr> 
					<th>Offer Code</th> 
					<th>CourseNo </th>
					<th>Description </th>
					<th>Day </th>
					<th>Class Start</th>
					<th>Class End</th>
					<th>Room No </th>
					<th>Faculty </th>
					<tr>
						<?php
						foreach($subjectoffering->getSubjects() as $row){
							$offercode = $row['offer_code'];
							$courseno = $row['subj_name'];
							$description = $row['subj_desc'];
							$day = $row['day'];
							$time1 = $row['time_start'];
							$time2 = $row['time_end'];
							$room = $row['room_no'];
							$fullname = $row['last_name'].", ".$row['first_name'];
							echo "
							<tr>
							<td> $offercode </td>
							<td> $courseno </td>
							<td> $description </td>
							<td> $day </td>
							<td> $time1 </td>
							<td> $time2 </td>
							<td> $room </td>
							<td> $fullname </td>
							</tr>

							";
						}

						?>

					</table>
				</form>
			</div>
			<a href='subjectoffering.php?add_offer=1'>Add Subject </a>

			<?php
		}
		if(isset($_GET['add_offer']) && $_GET['add_offer']==1){
			require_once('views/add_offer.php');
		}?>


		<?php
		if(isset($_GET['error']) && $_GET['error']==1){
			echo "<script> alert('offer code error')</script> ";
			require_once('views/add_offer.php');
		}


