<?php
include('views/header_ims.php');
session_start();
require_once('queries/dbconnect.php');
$db = new DBconnect();
$connection = $db->connect();

$account = new Accounts($connection);
//$account->getTeacher();


class Accounts{
	private $connection;
	function __construct($connection){
		$this->connection = $connection;
	}

	function getTeachers(){
		$query = "select p.person_id, p.last_name, p.first_name,ti.username
		from person p
		inner join teacher t on t.person_id=p.person_id
		left join touch_ims_accounts ti on ti.person_id=p.person_id";
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}
	
		return $results;
	}
	function displayTeachers(){
		$results = $this->getTeachers();

		$html = null;
		foreach($results as $row){
			$last_name = $row['last_name'];
			$first_name = $row['first_name'];
			$username = $row['username'];
			$person_id = $row['person_id'];
			if(empty($username))
				$username=
			"<a href='signup.php?lastname=$last_name&firstname=$first_name&person_id=$person_id&account_type=faculty'>create</a>";

			$html.= 
			"<tr>".
			"<td>$last_name, $first_name</td>".
			"<td style='text-align:center;'>$username</td>".
			"</tr>";
		}
		return $html;
	}

}

?>

<div>
<table>
	
	<tr><th>Name</th> <th>Account Username</th> </tr>
		<?php echo $account->displayTeachers() ?>

</table>

</div>

<style>
td{
	border-bottom: 1px solid black;
	padding-top: 10px;
}
</style>