<?php
session_start();
require_once('queries/dbconnect.php');
include('views/header_faculty.php');
require_once('room_transfer_form.php');


$db = new DBconnect();
$connection = $db->connect();
$roomtransferform = new RoomTransferForm($connection);





//$_GET['offercode'];
if(!empty($_SESSION['username']) && !empty($_GET['offercode'])){
	echo "<div id=transfer_form>";
	echo $roomtransferform->createView($_GET['offercode']);
	echo "</div>";
}

if(!empty($_POST['offercode']) && !empty($_POST['room']) && 
 !empty($_POST['reason']) && !empty($_POST['datepicker']) && !empty($_POST['datepicker2']) ){
	$roomtransferform->insertRequest($_POST['offercode'],$_POST['room'],
	$_POST['reason'],$_POST['datepicker'],$_POST['datepicker2']);

	
}


class RoomTransferForm{
	private $connection;
	function __construct($connection){
		$this->connection = $connection;
	}

	function generateReportID(){

		$request_id = rand( 1000000 ,99999999);

		$query="select request_id from touch_room_transfer_requests where request_id='$request_id'";
		$result = mysqli_query($this->connection,$query);
		$rows=mysqli_num_rows($result);
		if($rows>0)
			$this->generateReportID();

		else
			return $request_id;
		

	}

	function tryAvailableRoom($offercode,$room){
		//$subjectoffering = new SubjectOffering($this->connection);
		//$conflict = $subjectoffering->checkNotAvailableRoom($day,$room,$timestart,$timeend);
		$query = "select day, substr(time,-13,5) as time_start, substr(time,-5) as time_end 
		from subj_schedule where offer_code='$offercode'";
		//echo $query;
		$result = mysqli_query($this->connection,$query);
		$line = mysqli_fetch_assoc($result);
		$timestart = $line['time_start'];
		$timeend = $line['time_end'];
		


		$days = array();

	    if (strpos($line['day'],'M') !== false) {
	        $days[] = 'M';
	    }
	    if ( (strpos($line['day'],'T')!==false or $line['day']=="TTh") and $line['day']!='Th'   ) {
	        $days[] = 'T';
	    }
	    if (strpos($line['day'],'W') !== false) {
	        $days[] = 'W';
	    }

	    if (strpos($line['day'],'Th') !== false) {
	        $days[] = 'Th';
	    }
	    if (strpos($line['day'],'F') !== false) {
	        $days[] = 'F';
	    }
	    if (strpos($line['day'],'Sat') !== false) {
	        $days[] = 'Sat';
	    }


		$day_concat = "";
		$where="(";
		foreach($days as $row){
			$day_concat.=$row;
			$percent = '%';
			if($row=='T'){
				$row="T%' and day not like 'Th%' and day not like 'MTh%' and day not like 'MWTh%' and day not like 'WTh%";
				$percent = '';
			}
			$where.="day like '%$row".$percent."' or ";
		}
		$day=substr($where, 0,-3).")";

		$var = $this->checkNotAvailableRoom($day,$room,$timestart,$timeend);
		return $var;





	}

	function checkNotAvailableRoom($day,$room,$timestart,$timeend){
	
		$results = array();
		$schoolyear="2014-2015";
		$semester="2";
		$date =date('Y-m-d');
		$query = "select offer_code, day, room_no, time from subj_schedule
		where $day
		and room_no='$room' and
		offer_code not in
        (select offer_code from touch_room_transfer_requests where approval='approved')
        and
		(
		(('$timestart'>=substr(time,-13,5) and '$timestart' < substr(time,-5))
		or 
		('$timeend'>substr(time,-13,5) and '$timeend' <= substr(time,-5)))
		or
		((substr(time,-13,5)>='$timestart' and substr(time,-13,5)<'$timeend')
		or
		(substr(time,-5)<'$timestart' and substr(time,-5)<'$timeend')))
		"; //query except naa sa request

		$result = mysqli_query($this->connection,$query);
		
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}

		$query = "
			select rt.offer_code, ss.day, rt.room_no, ss.time from  subj_schedule ss
			inner join touch_room_transfer_requests rt on ss.offer_code = rt.offer_code
			where rt.room_no='$room' and rt.approval='approved'
			and
			(rt.date_effective <= '$date' and rt.date_effective_until >='$date')
		";
		$result = mysqli_query($this->connection,$query);
		
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}
		//print_r($results);

		if(sizeof($results)>0) //if naay conflict is false
			return 0;
		else
			return 1;
	}

	function insertRequest($offercode,$room,$reason,$dateeffective,$dateeffective2){
		//echo $this->tryAvailableRoom($offercode,$room);

		if($this->tryAvailableRoom($offercode,$room)==1){


			$request_id = $this->generateReportID();
			$query = "insert into touch_room_transfer_requests (request_id,offer_code,room_no,reason,date_effective,date_effective_until)
			values('$request_id','$offercode','$room','$reason','$dateeffective','$dateeffective2')";
			//echo $query;
			$result = mysqli_query($this->connection,$query);
			if($result){
				echo "<script>alert('Request Sent')</script>";
				header('location:room_transfer.php');
			}
			else
				echo "<script>alert('Request Failed')</script>";
		}
	}
	function getRooms(){
		return array('BCL1','BCL2','BCL3','BCL4','BCL5','BCL6','BCL7');
	}
	function selectRoom(){
		$rooms = $this->getRooms();

		$select = "";

		$select.="<select name = room>";
		$select.="<option disabled selected>select </option>";
		foreach($rooms as $room){
			$select.= "<option>$room</option>";
		}
	    $select.= "</select>";
	    return $select;
	}
	function getSchedule($offercode_target){
		
		$username = $_SESSION['username'];
		
		//echo $username;
		$query = "select p.last_name, p.first_name,p.person_id, t.teacher_id, ss.offer_code , ss.day, ss.time, ss.room_no
		from person p, teacher t, subj_schedule ss
		where p.person_id = t.person_id and t.teacher_id = ss.teacher_id and ss.offer_code='$offercode_target'
		and p.person_id = (select person_id from touch_ims_accounts where username='$username')
		";
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;

		}
		// echo "<pre>";
		// print_r($results);
		// echo "</pre>";
		return $results;
	}
	function createView($offercode_target){

		$room_target = $this->selectRoom();
		$schedule = $this->getSchedule($offercode_target);
		$offercode= $schedule[0]['offer_code'];
		$day = $schedule[0]['day'];
		$time =$schedule[0]['time'];
		$room = $schedule[0]['room_no'];
		//return 123;
		
		$string =  
		"<form method=post><table>".
		"<tr> <td>Offer Code</td>  		<td>$offercode</td> </tr>".
		"<tr> <td>Day</td>  			<td>$day</td></tr>".
		"<tr> <td>Time</td>  			<td>$time</td></tr>".
		"<tr> <td>From</td>  			<td> $room</td></tr>".
		"<tr> <td>To</td>  				<td>$room_target</td></tr>".
		"<tr> <td>Reason</td>  			<td><input type=textarea name=reason /> </td></tr>".
		"<tr> <td>Take effect from</td> <td><input id=datepicker name=datepicker /> </td></tr>".
		"<tr> <td>Take effect to</td>  	<td><input id=datepicker2 name=datepicker2 /> </td></tr>".

		"</table>".
		"<input type=hidden name='offercode' value='$offercode' />".
		"<input id='btn_submit' type=submit value=request /> </form>";
		return $string;
		
	}
}
?>


<!-- <meta charset="utf-8">
<title>jQuery UI Datepicker - Default functionality</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"> -->
<script>
$(function() {
  $( "#datepicker,#datepicker2" ).datepicker({
    dateFormat: "yy-mm-dd"
  });

});
</script>

<style>
#transfer_form{
	outline:1px solid green;
	width:300px;
	margin:auto;
	padding:0px 150px 0px 150px;
}
td{
	padding:10px;
}
#btn_submit{
	margin-left:120px;
}

</style>

